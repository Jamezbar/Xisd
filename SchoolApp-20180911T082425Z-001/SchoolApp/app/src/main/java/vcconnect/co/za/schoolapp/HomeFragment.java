package vcconnect.co.za.schoolapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class HomeFragment extends Fragment {

    TextView tvNews;
    TextView tvSched;
    TextView tvLive;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_home, container, false);
        tvNews =v.findViewById(R.id.tvNews);
        News();
        tvSched = v.findViewById(R.id.tvSchedule);
        Schedule();
        tvLive = v.findViewById(R.id.tvLiveGame);
        CurrentGame();

        return v;
    }

    private void Schedule()
    {
        FirebaseDatabase fbd = FirebaseDatabase.getInstance();
        DatabaseReference Myref = fbd.getReference("Matches");
        Myref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String Mname = dataSnapshot.child("Match1").child("Team").getValue() + " Team " + dataSnapshot.child("Match1").child("Sport").getValue();
                String Date = (String) dataSnapshot.child("Match1").child("Time").getValue();
                String Location = (String) dataSnapshot.child("Match1").child("Location").getValue();
                tvSched.setTextColor(Color.BLACK);
                tvSched.setGravity(Gravity.CENTER_HORIZONTAL);
                tvSched.setText(Mname + " at " + Date + " On " + Location);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void News()
    {
        FirebaseDatabase fbd = FirebaseDatabase.getInstance();
        DatabaseReference Myref = fbd.getReference("News");
        Myref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String Date = (String) dataSnapshot.child("Time").getValue();
                String News = (String) dataSnapshot.child("Text").getValue();
                tvNews.setText(Date + ": " + News +"\n");
                tvNews.setTextColor(Color.BLACK);
                tvNews.setGravity(Gravity.CENTER_HORIZONTAL);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void CurrentGame()
    {
        FirebaseDatabase fbd = FirebaseDatabase.getInstance();
        DatabaseReference Myref = fbd.getReference("Matches");
        Myref.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                String sprt = dataSnapshot.child("Match1").child("Team").getValue() + " Team " + dataSnapshot.child("Match1").child("Sport").getValue();
                String SchoolOne = dataSnapshot.child("Match1").child("S1").getValue() + ": " + dataSnapshot.child("Match1").child("S1Score").getValue();
                String SchoolTwo = dataSnapshot.child("Match1").child("S2").getValue() + ": " + dataSnapshot.child("Match1").child("S2Score").getValue();
                String Display = sprt + "\n" + SchoolOne + "----" + SchoolTwo;
                tvLive.setTextColor(Color.BLACK);
                tvLive.setGravity(Gravity.CENTER);
                tvLive.setText(Display);


            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }










}
