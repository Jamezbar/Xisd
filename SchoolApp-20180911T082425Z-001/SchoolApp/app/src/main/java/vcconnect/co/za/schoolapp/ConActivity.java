package vcconnect.co.za.schoolapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
<<<<<<< HEAD
=======
import android.nfc.Tag;
>>>>>>> master
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

public class ConActivity extends AppCompatActivity {
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!Connected(ConActivity.this))buildD(ConActivity.this).show();
        setContentView(R.layout.activity_con);
        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF150B48")));
        BottomNavigationView bottomNavigationView = findViewById(R.id.btm_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListner);
        getSupportFragmentManager().beginTransaction().replace(R.id.Frag_container,new HomeFragment()).commit();

    }

    public BottomNavigationView.OnNavigationItemSelectedListener navListner = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment slcFrag=null;
            switch (item.getItemId())
            {
                case R.id.nav_home:
                    slcFrag=new HomeFragment();
                    break;

                case R.id.nav_Schedule:
                    slcFrag=new ScheduleFragment();
                    break;
                case R.id.nav_live:
                    slcFrag=new LiveFragment();
                    break;
                case R.id.nav_favourite:
                    slcFrag=new FavouriteFragment();
                    break;
                case R.id.nav_bracketa:
                    slcFrag=new BracketsFragment();
                    break;

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.Frag_container,
                    slcFrag).commit();
            return true;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings,menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
     switch (item.getItemId())
     {
         case R.id.Settings:
             Intent i = new Intent(this,SettingActivity.class);
             startActivity(i);
             break;
     }
     return true;
    }
     public boolean Connected(Context context)
    {
        ConnectivityManager ConM=(ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConM.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnectedOrConnecting())
        {
            android.net.NetworkInfo wifi=ConM.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo data=ConM.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if((data != null|| networkInfo.isConnectedOrConnecting()) || (wifi.isConnectedOrConnecting()))return true;
            return false;
        }else
        {
            return false;
        }

    }
    public AlertDialog.Builder buildD(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }


}

