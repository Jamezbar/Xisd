package vcconnect.co.za.schoolapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.jar.Attributes;


/**
 * A simple {@link Fragment} subclass.
 */
public class BracketsFragment extends Fragment {

    TextView tvS1,tvS2,tvS3,tvS4,tvS5,tvS6,tvS7,tvS8,tvb1w1,tvb1w2,tvb2w1,tvb2w2,tvf1,tvf2;
    FirebaseDatabase fbd = FirebaseDatabase.getInstance();
    public BracketsFragment() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_brackets, container, false);
        tvS1 = v.findViewById(R.id.tvS1);
        tvS2 = v.findViewById(R.id.tvS2);
        tvS3 = v.findViewById(R.id.tvS3);
        tvS4 = v.findViewById(R.id.tvS4);
        tvS5 = v.findViewById(R.id.tvS5);
        tvS6 = v.findViewById(R.id.tvS6);
        tvS7 = v.findViewById(R.id.tvS7);
        tvS8 = v.findViewById(R.id.tvS8);
        tvb1w1 = v.findViewById(R.id.tvb1w1);
        tvb1w2 = v.findViewById(R.id.tvb1w2);
        tvb2w1 = v.findViewById(R.id.tvb2w1);
        tvb2w2 = v.findViewById(R.id.tvb2w2);
        tvf1 = v.findViewById(R.id.tvf1);
        tvf2 = v.findViewById(R.id.tvf2);
        DrawBracket();

        return v;
    }

    private void DrawBracket()
    {

        DatabaseReference Myref = fbd.getReference("Schools");
        Myref.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                String s1, s2, s3, s4, s5, s6, s7, s8; //School names
                s1 = dataSnapshot.child("S1").child("Name").getValue().toString();
                s2 = dataSnapshot.child("S2").child("Name").getValue().toString();
                s3 = dataSnapshot.child("S3").child("Name").getValue().toString();
                s4 = dataSnapshot.child("S4").child("Name").getValue().toString();


                s5 = dataSnapshot.child("S5").child("Name").getValue().toString();
                s6 = dataSnapshot.child("S6").child("Name").getValue().toString();
                s7 = dataSnapshot.child("S7").child("Name").getValue().toString();
                s8 = dataSnapshot.child("S8").child("Name").getValue().toString();

                tvS1.setText(s1);
                tvS2.setText(s5);
                tvS3.setText(s2);
                tvS4.setText(s6);
                tvS5.setText(s3);
                tvS6.setText(s7);
                tvS7.setText(s4);
                tvS8.setText(s8);
                Bracket1();
                Bracket2();
                Final();

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

    public void Bracket1()
    {
        DatabaseReference ref = fbd.getReference("Bracket");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String b1s1sc = dataSnapshot.child("Bracket1").child("SchoolOneScore").getValue().toString();
                String b1s2sc = dataSnapshot.child("Bracket1").child("SchoolTwoScore").getValue().toString();
                String b1s3sc = dataSnapshot.child("Bracket1").child("SchoolThreeScore").getValue().toString();
                String b1s4sc = dataSnapshot.child("Bracket1").child("SchoolFourScore").getValue().toString();
                String b1w1 = "Winner of first game";
                String b1w2 = "Winner of second game";

                if( Integer.parseInt(b1s1sc) > 0 )
                {
                    b1w1 = dataSnapshot.child("Bracket1").child("SchoolOne").getValue().toString();
                    if(Integer.parseInt(b1s2sc) > 0)
                    {
                        b1w2 = dataSnapshot.child("Bracket1").child("SchoolTwo").getValue().toString();
                    }
                    if(Integer.parseInt(b1s3sc) > 0)
                    {
                        b1w2 = dataSnapshot.child("Bracket1").child("SchoolThree").getValue().toString();
                    }else{
                        b1w2 = dataSnapshot.child("Bracket1").child("SchoolFour").getValue().toString();
                    }
                }
                else if(Integer.parseInt(b1s2sc) > 0)
                {
                    b1w1 = dataSnapshot.child("Bracket1").child("SchoolTwo").getValue().toString();

                    if(Integer.parseInt(b1s3sc) > 0)
                    {
                        b1w2 = dataSnapshot.child("Bracket1").child("SchoolThree").getValue().toString();
                    }else{
                        b1w2 = dataSnapshot.child("Bracket1").child("SchoolFour").getValue().toString();
                    }
                }
                else
                {
                    b1w1 = dataSnapshot.child("Bracket1").child("SchoolThree").getValue().toString();
                    b1w2 = dataSnapshot.child("Bracket1").child("SchoolFour").getValue().toString();
                }
                tvb1w1.setText(b1w1);
                tvb1w2.setText(b1w2);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void Bracket2()
    {
        DatabaseReference ref = fbd.getReference("Bracket");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
        String b2s1sc = dataSnapshot.child("Bracket2").child("SchoolOneScore").getValue().toString();
        String b2s2sc = dataSnapshot.child("Bracket2").child("SchoolTwoScore").getValue().toString();
        String b2s3sc = dataSnapshot.child("Bracket2").child("SchoolThreeScore").getValue().toString();
        String b2s4sc = dataSnapshot.child("Bracket2").child("SchoolFourScore").getValue().toString();
        String b2w1 = "Winner of third game";
        String b2w2 = "Winner of fourth game";

        if( Integer.parseInt(b2s1sc) > 0 )
        {
            b2w1 = dataSnapshot.child("Bracket2").child("SchoolOne").getValue().toString();
            if(Integer.parseInt(b2s2sc) > 0)
            {
                b2w2 = dataSnapshot.child("Bracket2").child("SchoolTwo").getValue().toString();
            }
            if(Integer.parseInt(b2s3sc) > 0)
            {
                b2w2 = dataSnapshot.child("Bracket2").child("SchoolThree").getValue().toString();
            }else{
                b2w2 = dataSnapshot.child("Bracket2").child("SchoolFour").getValue().toString();
            }
        }
        else if(Integer.parseInt(b2s2sc) > 0)
        {
            b2w1 = dataSnapshot.child("Bracket2").child("SchoolTwo").getValue().toString();

            if(Integer.parseInt(b2s3sc) > 0)
            {
                b2w2 = dataSnapshot.child("Bracket2").child("SchoolThree").getValue().toString();
            }else{
                b2w2 = dataSnapshot.child("Bracket2").child("SchoolFour").getValue().toString();
            }
        }
        else
        {
            b2w1 = dataSnapshot.child("Bracket2").child("SchoolThree").getValue().toString();
            b2w2 = dataSnapshot.child("Bracket2").child("SchoolFour").getValue().toString();
        }

        tvb2w1.setText(b2w1);
        tvb2w2.setText(b2w2);
    }

    @Override
    public void onCancelled(DatabaseError databaseError)
    {
    }
});
    }
    public void Final()
    {
        DatabaseReference ref = fbd.getReference("Bracket");
        ref.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                int b1s1sc = Integer.parseInt(dataSnapshot.child("Bracket1").child("SchoolOneScore").getValue().toString());
                int b1s2sc = Integer.parseInt(dataSnapshot.child("Bracket1").child("SchoolTwoScore").getValue().toString());
                int b1s3sc = Integer.parseInt(dataSnapshot.child("Bracket1").child("SchoolThreeScore").getValue().toString());
                int b1s4sc = Integer.parseInt(dataSnapshot.child("Bracket1").child("SchoolFourScore").getValue().toString());
                int b2s1sc = Integer.parseInt(dataSnapshot.child("Bracket2").child("SchoolOneScore").getValue().toString());
                int b2s2sc = Integer.parseInt(dataSnapshot.child("Bracket2").child("SchoolTwoScore").getValue().toString());
                int b2s3sc = Integer.parseInt(dataSnapshot.child("Bracket2").child("SchoolThreeScore").getValue().toString());
                int b2s4sc = Integer.parseInt(dataSnapshot.child("Bracket2").child("SchoolFourScore").getValue().toString());
                tvf1.setText("Winner of game 1");
                tvf2.setText("Winner of game 2");

                if(b1s1sc > 1)
                {
                    tvf1.setText(dataSnapshot.child("Bracket1").child("SchoolOne").getValue().toString());
                }else if(b1s2sc > 1)
                {
                    tvf1.setText(dataSnapshot.child("Bracket1").child("SchoolTwo").getValue().toString());
                }else if(b1s3sc > 1)
                {
                    tvf1.setText(dataSnapshot.child("Bracket1").child("SchoolThree").getValue().toString());
                }else if(b1s4sc > 1)
                {
                    tvf1.setText(dataSnapshot.child("Bracket1").child("SchoolFour").getValue().toString());
                }

                if(b2s1sc > 1)
                {
                    tvf2.setText(dataSnapshot.child("Bracket2").child("SchoolOne").getValue().toString());
                }else if(b2s2sc > 1)
                {
                    tvf2.setText(dataSnapshot.child("Bracket2").child("SchoolTwo").getValue().toString());

                }else if(b2s3sc > 1)
                {
                    tvf2.setText(dataSnapshot.child("Bracket2").child("SchoolThree").getValue().toString());
                }else if(b2s4sc > 1)
                {
                    tvf2.setText(dataSnapshot.child("Bracket2").child("SchoolFour").getValue().toString());
                }




            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });



    }
}
