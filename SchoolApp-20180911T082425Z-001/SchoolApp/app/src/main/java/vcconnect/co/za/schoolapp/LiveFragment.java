package vcconnect.co.za.schoolapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class LiveFragment extends Fragment {

    String pastMatches = "";
    TextView tvPrev;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_live, container, false);
        tvPrev = v.findViewById(R.id.tvPrevScoers);
        tvPrev.setTextColor(Color.BLACK);
        tvPrev.setText("");
        CurrentGames();
        PreviousGames();


        return v;
    }

    private void CurrentGames()
    {

    }

    private void PreviousGames()
    {
        DatabaseReference Statusref = FirebaseDatabase.getInstance().getReference();
        Query statusQuery = Statusref.child("Matches").orderByChild("Status").equalTo(0);
        statusQuery.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.exists())
                {
                    for (DataSnapshot MatchSnap : dataSnapshot.getChildren())
                    {

                        pastMatches = MatchSnap.child("Team").getValue().toString() + " Team " + MatchSnap.child("Sport").getValue().toString();
                        int s1sc = MatchSnap.child("S1Score").getValue(Integer.class);
                        int s2sc = MatchSnap.child("S2Score").getValue(Integer.class);
                        if (s1sc > s2sc) {
                            pastMatches = pastMatches + " " + MatchSnap.child("S1").getValue().toString() + " Won the game against " + MatchSnap.child("S2").getValue().toString() + " With a score of " + s1sc + ":" + s2sc + " ";
                        } else {
                            pastMatches = pastMatches + " " + MatchSnap.child("S2").getValue().toString() + " Won the game against " + MatchSnap.child("S1").getValue().toString() + " With a score of " + s2sc + ":" + s1sc + " ";
                        }
                        tvPrev.append(pastMatches + "\n\n");
                    }
                }else
                {
                    tvPrev.setText("w");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

}
