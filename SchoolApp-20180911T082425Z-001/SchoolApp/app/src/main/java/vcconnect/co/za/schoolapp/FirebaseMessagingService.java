package vcconnect.co.za.schoolapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void onMessageReceived(RemoteMessage remMessage)
    {
        Intent i = new Intent(this,ConActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent =PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_ONE_SHOT);
        Notification.Builder build = new Notification.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("New stuff")
                .setContentText(remMessage.getNotification().getBody())
                .setSmallIcon(R.mipmap.ic_crest)
                .setContentIntent(pendingIntent);
        NotificationManager nManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(0,build.build());

    }
}

