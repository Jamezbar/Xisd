package vcconnect.co.za.schoolapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment {


String futureMatches;
TextView tvschedule;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_schedule, container, false);
        tvschedule = v.findViewById(R.id.tvSched);
        tvschedule.setTextColor(Color.BLACK);
        futureGame();
        return v;
    }

    private void futureGame()
    {
        DatabaseReference Statusref = FirebaseDatabase.getInstance().getReference();
        Query statusQuery = Statusref.child("Matches").orderByChild("Status").equalTo(2);
        statusQuery.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.exists())
                {
                    for (DataSnapshot MatchSnap : dataSnapshot.getChildren())
                    {
                        futureMatches = MatchSnap.child("Team").getValue().toString() + " Team " + MatchSnap.child("Sport").getValue().toString() + " at " + MatchSnap.child("Time").getValue(String.class) + " on " + MatchSnap.child("Location").getValue(String.class);
                        tvschedule.setText(futureMatches + "\n\n");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }


}
