package vcconnect.co.za.schoolapp;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

public class SettingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_settings);
        android.app.Fragment frag = new Settings_Frag();
        FragmentTransaction FragT = getFragmentManager().beginTransaction();
        if(savedInstanceState== null)
        {
            FragT.add(R.id.setting_frag,frag,"setting_frag");
            FragT.commit();
        }
        else
        {
            frag = getFragmentManager().findFragmentByTag("setting_frag");
        }
    }

    public static class Settings_Frag extends PreferenceFragment {
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settingspref);




        }
    }
}
