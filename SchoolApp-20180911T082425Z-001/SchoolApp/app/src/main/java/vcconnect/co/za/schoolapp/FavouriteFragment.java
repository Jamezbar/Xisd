package vcconnect.co.za.schoolapp;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavouriteFragment extends Fragment {

    FirebaseDatabase fbd = FirebaseDatabase.getInstance();
    String s1,s2,s3,s4,s5,s6,s7,s8; // school Names
    TextView tvSchoolOne,tvSchoolTwo,tvSchoolThree,tvSchoolFour,tvSchoolFive,tvSchoolSix,tvSchoolSeven,tvSchoolEight; //School Text VIews
    ToggleButton Tb1,Tb2,Tb3,Tb4,Tb5,Tb6,Tb7,Tb8; // Favourite Toggle Buttonsa
    Boolean f1 = false,f2= false,f3= false,f4= false,f5= false,f6= false,f7= false,f8= false; // to say if favourite or not
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_favourite, container, false);
        tvSchoolOne = v.findViewById(R.id.tvSchool1);
        tvSchoolTwo = v.findViewById(R.id.tvSchool2);
        tvSchoolThree = v.findViewById(R.id.tvSchool3);
        tvSchoolFour = v.findViewById(R.id.tvSchool4);
        tvSchoolFive = v.findViewById(R.id.tvSchool5);
        tvSchoolSix = v.findViewById(R.id.tvSchool6);
        tvSchoolSeven = v.findViewById(R.id.tvSchool7);
        tvSchoolEight = v.findViewById(R.id.tvSchool8);
        Tb1 = v.findViewById(R.id.tb1);
        Tb2 = v.findViewById(R.id.tb2);
        Tb3 = v.findViewById(R.id.tb3);
        Tb4 = v.findViewById(R.id.tb4);
        Tb5 = v.findViewById(R.id.tb5);
        Tb6 = v.findViewById(R.id.tb6);
        Tb7 = v.findViewById(R.id.tb7);
        Tb8 = v.findViewById(R.id.tb8);
        FetchSchool();
        FetchFavourite(getContext());

        Tb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f1 = true;
                }else
                {
                    f1 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f2 = true;
                }else
                {
                    f2 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f3 = true;
                }else
                {
                    f3 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f4 = true;
                }else
                {
                    f4 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f5 = true;
                }else
                {
                    f5 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f6 = true;
                }else
                {
                    f6 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f7 = true;
                }else
                {
                    f7 = false;
                }
                WriteToFile(getContext());
            }
        });
        Tb8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    f8 = true;
                }else
                {
                    f8 = false;
                }
                WriteToFile(getContext());
            }
        });

        return v;
    }

    private void WriteToFile(Context context)
    {
        String ToWrite = "";
        if(f1)
        {
            ToWrite = ToWrite +",S1";
        }
        if(f2)
        {
            ToWrite = ToWrite +",S2";
        }
        if(f3)
        {
            ToWrite = ToWrite +",S3";
        }
        if(f4)
        {
            ToWrite = ToWrite +",S4";
        }
        if(f5)
        {
            ToWrite = ToWrite +",S5";
        }
        if(f6)
        {
            ToWrite = ToWrite +",S6";
        }
        if(f7)
        {
            ToWrite = ToWrite +",S7";
        }
        if(f8)
        {
            ToWrite = ToWrite +",S8";
        }
        try
        {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("Favourites.txt", context.MODE_PRIVATE));
            outputStreamWriter.write(ToWrite);
            outputStreamWriter.close();
        }catch (Exception e)
        {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void FetchFavourite(Context context)
    {
        String Favourites = "";

        try {
            InputStream inputStream = context.openFileInput("Favourites.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                Favourites = stringBuilder.toString();
            }
        }
        catch (Exception e) {
            Log.e("login activity", "File not found: " + e.toString());
        }

        String schools[] = Favourites.split(",");
        for (int i = 0; i < schools.length;i++)
        {
            if(schools[i].equals("S1"))
            {
                f1 = true;
                Tb1.setChecked(true);
            }
            if(schools[i].equals("S2"))
            {
                f2 = true;
                Tb2.setChecked(true);
            }
            if(schools[i].equals("S3"))
            {
                f3 = true;
                Tb3.setChecked(true);
            }
            if(schools[i].equals("S4"))
            {
                f4 = true;
                Tb4.setChecked(true);
            }
            if(schools[i].equals("S5"))
            {
                f5 = true;
                Tb5.setChecked(true);
            }
            if(schools[i].equals("S6"))
            {
                f6 = true;
                Tb6.setChecked(true);
            }
            if(schools[i].equals("S7"))
            {
                f7 = true;
                Tb7.setChecked(true);
            }
            if(schools[i].equals("S8"))
            {
                f8 = true;
                Tb8.setChecked(true);
            }
        }

    }


    private void FetchSchool()
    {
        DatabaseReference ref = fbd.getReference("Schools");
        ref.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                s1 = dataSnapshot.child("S1").child("Name").getValue().toString();
                s2 = dataSnapshot.child("S2").child("Name").getValue().toString();
                s3 = dataSnapshot.child("S3").child("Name").getValue().toString();
                s4 = dataSnapshot.child("S4").child("Name").getValue().toString();
                s5 = dataSnapshot.child("S5").child("Name").getValue().toString();
                s6 = dataSnapshot.child("S6").child("Name").getValue().toString();
                s7 = dataSnapshot.child("S7").child("Name").getValue().toString();
                s8 = dataSnapshot.child("S8").child("Name").getValue().toString();
                tvSchoolOne.setText(s1);
                tvSchoolTwo.setText(s2);
                tvSchoolThree.setText(s3);
                tvSchoolFour.setText(s4);
                tvSchoolFive.setText(s5);
                tvSchoolSix.setText(s6);
                tvSchoolSeven.setText(s7);
                tvSchoolEight.setText(s8);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

}
